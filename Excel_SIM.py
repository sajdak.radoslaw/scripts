from openpyxl import load_workbook, Workbook

#get handle on existing file
sheet_dict_1 = load_workbook(filename='Arkusz1.xlsx')
sheet_one_column_2 = load_workbook(filename='Arkusz2.xlsx')
sheet_one_column_3 = load_workbook(filename='Arkusz3.xlsx')
#get active worksheet or wk['some_worksheet']
work_sheet_1 = sheet_dict_1.active
work_sheet_2 = sheet_one_column_2.active
work_sheet_3 = sheet_one_column_3.active

##### Save file #####
wb = Workbook()
xl_file = 'Lost_file.xlsx'
sheet_save = wb.active

#### SETUP ####


#### Load and joint ID to IMSI ####
def connect_two_columns(work_sheet, column_1, c1_type, column_2, c2_type, MIN, MAX):
    """
        As a result You will get dictionary and sum (what is number of values in dictionary).

        Dictionary contain column_1 as Keys, and column_2 as Keys' list of values
        @Input work_sheet - sheet to analysis
        @Input column_1 - column to analysis eg. 'A'
        @Input c1_type - type of data in column 1 eg. str/int
        @Input column_2 - column to analysis eg. 'A'
        @Input c2_type - type of data in column 1 eg. str/int 
        @Input MIN - smallest value of the row to analysis
        @Input MAX - biggest value of the row to analysis

        @Return dictionary, sum
            dictionary Keys are unique values of column_1
            dictionary Values are lists
            sum is total value of items in dictionary

    """
    sheet = {}
    local_sum = 0
    for t in range(MIN,MAX + 1): 
        range_id = str(column_1)+str(t)
        range_imsi = str(column_2)+str(t)
        if work_sheet[range_id].value is not None and work_sheet[range_imsi].value is not None \
            and type(work_sheet[range_id].value) is c1_type and type(work_sheet[range_imsi].value) is c2_type:
            try:
                sheet[str(work_sheet[range_id].value)].append(work_sheet[range_imsi].value)
            except:       
                sheet[str(work_sheet[range_id].value)] = []
                sheet[str(work_sheet[range_id].value)].append(work_sheet[range_imsi].value)
            local_sum +=1
    return sheet,local_sum

def read_one_column(work_sheet,column,MIN,MAX):
    """
    Reading just one column from the file. 

    @Input work_sheet - sheet to analysis
    @Input column - column to read
    @Input MIN - smallest value of the row to read
    @Input MAX - biggest value of the row to read

    """
    sheet = []
    local_sum = 0
    for t in range(MIN,MAX + 1):
        range_imsi = str(column)+str(t)
        if work_sheet[range_imsi].value is not None:
            sheet.append(work_sheet[range_imsi].value)
            local_sum += 1
    return sheet,local_sum


if __name__ == "__main__":
    dict_1, length_dict_1 = connect_two_columns(work_sheet_1,'E',str,'G',int,5,120)
    one_column_2, length_one_column_2 = read_one_column(work_sheet_2,'I',4,300)
    one_column_3, length_one_column_3 = read_one_column(work_sheet_3,'E',2,124)

    lost = []
    """
        Here You can check if values from sheet1 are present in dictionary. If not, You'll get list of this items
    """
    for i in range(len(one_column_2)):
        found = 0
        for dic in dict_1:
            if int(one_column_2[i]) in dict_1[dic]:
                found = 1
                break
            else:
                found = 0

        if one_column_2[i] not in lost and found is 0: lost.append(one_column_2[i])

    """
        Optionally you can check for lost items in third sheet. Found items will be printed and removed from "lost" list.

    """
    for i in range(len(lost)):
        if lost[i] in one_column_3:
            print("Found %s in 3rd sheet!" % lost[i])
            lost.remove(lost[i])

        else:
            pass
    lost.sort()
    if True: # Set it to False if you don't want to print lost list to std output
        for i in lost:
            print(i)
        print(len(lost))

    #### Saving to file ####
    sheet_save.append(["Title"]) # First row
    for row in range(len(lost)):
        sheet_save.append([lost[row]])
    wb.save(filename = xl_file)
