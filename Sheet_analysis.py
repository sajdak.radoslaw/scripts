from Excel_SIM import read_one_column, connect_two_columns
from openpyxl import load_workbook, Workbook
"""
    With this file You can look for card in many sheets in the same time.
    
    As output You'll get two new sheets. With found and lost cards.
    You should set all columns to search and copy manually
"""
#get handle on existing file
sheet_one_column_1 = load_workbook(filename='Arkusz1.xlsx')
sheet_one_column_2 = load_workbook(filename='Arkusz2.xlsx')
sheet_one_column_3 = load_workbook(filename='Arkusz3.xlsx')
#get active worksheet or wk['some_worksheet']
work_sheet_1 = sheet_one_column_1.active
work_sheet_2 = sheet_one_column_2.active
work_sheet_3 = sheet_one_column_3.active

##### Save file #####
wb = Workbook()
xl_file = 'Lost_file.xlsx'
sheet_lost = wb.active

wb_have = Workbook()
xl_file2 = 'Have_file.xlsx'
sheet_have = wb_have.active
sheet_have.append(["Number", "IMSI", "PUK", "PUK2"])  # First row

if __name__ == "__main__":

    ## We will collect this data from root sheet ##
    col, col_len = read_one_column(work_sheet_1,'E',4,203)
    imsi, foo = read_one_column(work_sheet_1, 'I', 4, 203)
    puk, foo = read_one_column(work_sheet_1, 'F', 4, 203)
    puk2, foo = read_one_column(work_sheet_1, 'G', 4, 203)
    lost = []
    while True:
        card = "000000000000000" + input("Card number:") ## You can use shorter input if you have similar numbers
        if len(card) == len("0000000000000000849"):
            if card in col:
                print("Ok, have it!")
                sheet_have.append([card, imsi[col.index(card)], puk[col.index(card)], puk2[col.index(card)]])

                imsi.remove(imsi[col.index(card)])
                puk.remove(puk[col.index(card)])
                puk2.remove(puk2[col.index(card)])
                # remove data connected with found card above
                col.remove(card)
            else:
                if card.find("done") > 0:
                    print("Leaving")
                else:
                    print("SIM lost!") # Sim not found in root sheet


        else:
            print("WRONG NUMBER!")
        if card.find("done") > 0:
            #### Saving to file ####
            sheet_lost.append(["Number", "IMSI","PUK","PUK2"])  # First row
            for row in range(len(col)):
                sheet_lost.append([col[row], imsi[row], puk[row], puk2[row]])
            wb.save(filename=xl_file)
            wb_have.save(filename=xl_file2)
            exit(0)